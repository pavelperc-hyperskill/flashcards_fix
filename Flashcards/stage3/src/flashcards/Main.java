package flashcards;

import java.util.Scanner;



public class Main {
    
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.println("Input the number of cards:");
        int num = scanner.nextInt();
        scanner.nextLine();
        
        String[] cards = new String[num];
        String[] defs = new String[num];
        
        for (int i = 0; i < num; i++) {
            System.out.println(String.format("The card #%d:", i + 1));
            cards[i] = scanner.nextLine();
            System.out.println(String.format("The definition of the card %d#:", i + 1));
            defs[i] = scanner.nextLine();
        }
        
        for (int i = 0; i < num; i++) {
            System.out.println(String.format("Print the definition of \"%s\":", cards[i]));
            var ans = scanner.nextLine();
            
            if (ans.equals(defs[i])) {
                System.out.println("Correct answer.");
            } else {
                System.out.println(String.format("Wrong answer. (The correct one is \"%s\".)", defs[i]));
            }
        }
        
    }
}
