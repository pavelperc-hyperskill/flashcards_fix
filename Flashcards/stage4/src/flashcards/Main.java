package flashcards;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
    
        System.out.println("Input the number of cards:");
        int num = scanner.nextInt();
        scanner.nextLine();
        
        var cardToDef = new LinkedHashMap<String, String>();
        var defToCard = new LinkedHashMap<String, String>();
        
        // filling cards
        for (int i = 0; i < num; i++) {
            System.out.println(String.format("The card #%d:", i + 1));
            var newCard = scanner.nextLine();
            while (cardToDef.containsKey(newCard)) {
                System.out.println(String.format("The card \"%s\" already exists. Try again:", newCard));
                newCard = scanner.nextLine();
            }
            
            System.out.println(String.format("The definition of the card %d#:", i + 1));
            var newDef = scanner.nextLine();
            
            while (defToCard.containsKey(newDef)) {
                System.out.println(String.format("The definition \"%s\" already exists. Try again:", newDef));
                newDef = scanner.nextLine();
            }
            cardToDef.put(newCard, newDef);
            defToCard.put(newDef, newCard);
        }
        
        // asking questions
    
        for (var entry : cardToDef.entrySet()) {
            var card = entry.getKey();
            var def = entry.getValue();
            
            System.out.println(String.format("Print the definition of \"%s\":", card));
            var ans = scanner.nextLine();
        
            if (ans.equals(def)) {
                System.out.println("Correct answer.");
            } else {
                var otherCard = defToCard.get(ans);
                
                if (otherCard != null) {
                    System.out.println(String.format("Wrong answer. (The correct one is \"%s\", " +
                            "you've just written the definition of \"%s\".)", def, otherCard));
                } else {
                    System.out.println(String.format("Wrong answer. (The correct one is \"%s\".)", def));
                }
            }
        }
    }
}
