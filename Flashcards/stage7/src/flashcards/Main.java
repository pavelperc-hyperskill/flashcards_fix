package flashcards;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    
    private static Map<String, String> cardToDefinition = new LinkedHashMap<>();
    private static Map<String, String> definitionToCard = new LinkedHashMap<>();
    
    private static Map<String, Integer> mistakeCount = new HashMap<>();
    
    private static List<String> logContent = new ArrayList<>();
    
    private static void addToMap(String card, String definition) {
        cardToDefinition.put(card, definition);
        definitionToCard.put(definition, card);
    }
    
    private static void addCard(Scanner sc) {
        addAndPrint("The card:\n");
        String card = addToLog(sc.nextLine().trim());
        addAndPrint("The definition of the card:\n");
        String definition = addToLog(sc.nextLine().trim());
        
        if (cardToDefinition.containsKey(card)) {
            addAndPrint(String.format("Card \"%s\" already exists.\n", card));
            return;
        }
        addToMap(card, definition);
        mistakeCount.put(card, 0);
        addAndPrint(String.format("The pair (\"%s\":\"%s\") has been added.\n", card, definition));
    }
    
    private static void removeCard(Scanner sc) {
        addAndPrint("The card:\n");
        String card = addToLog(sc.nextLine().trim());
        if (!cardToDefinition.containsKey(card)) {
            addAndPrint(String.format("Can't remove \"%s\": there is no such card.\n", card));
            return;
        }
        String definition = cardToDefinition.get(card);
        addAndPrint(String.format("Card \"%s\" has been removed.\n", card));
        
        mistakeCount.remove(card);
        cardToDefinition.remove(card);
        definitionToCard.remove(definition);
    }
    
    private static void importCards(Scanner sc) {
        addAndPrint("File name:\n");
        quitImport(sc.nextLine().trim());
    }
    
    private static void quitImport(String filePath) {
        File file = new File(filePath);
        int count = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String card = br.readLine();
            while (card != null) {
                String definition = br.readLine();
                addToMap(card, definition);
                mistakeCount.put(card, Integer.parseInt(br.readLine()));
                card = br.readLine();
                count++;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        addAndPrint(String.format("%d cards have been loaded.\n", count));
    }
    
    
    private static void exportCards(Scanner sc) {
        addAndPrint("File name:\n");
        quitExport(sc.nextLine());
    }
    
    private static void quitExport(String filePath) {
        File file = new File(filePath);
        int count = 0;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (Map.Entry<String, String> entry : cardToDefinition.entrySet()) {
                String card = entry.getKey();
                String def = entry.getValue();
                try {
                    bw.write(card + "\n");
                    bw.write(def + "\n");
                    bw.write(mistakeCount.get(card) + "\n");
                    count++;
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        addAndPrint(String.format("%d cards have been saved.\n", count));
    }
    
    
    private static Random random = new Random();
    
    private static void askUser(Scanner scanner) {
        List<String> keys = new ArrayList<>(cardToDefinition.keySet());
        System.out.println("How many times to ask?");
        int n = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < n; i++) {
            String card = keys.get(random.nextInt(keys.size()));
            System.out.println("Print the definition of \"" + card + "\":");
            String s = scanner.nextLine();
            if (cardToDefinition.get(card).equals(s)) {
                System.out.println("Correct answer. ");
            } else {
                if (definitionToCard.containsKey(s)) {
                    System.out.println("Wrong answer. (The correct one is \"" + cardToDefinition.get(card) + "\", you've just written the definition of \"" + definitionToCard.get(s) + "\" card. ");
                } else {
                    System.out.println("Wrong answer. (The correct one is \"" + cardToDefinition.get(card) + "\"). ");
                }
                mistakeCount.put(card, mistakeCount.get(card) + 1);
            }
        }
    }
    
    private static void printLog(Scanner sc) {
        addAndPrint("File name:\n");
        File file = new File(addToLog(sc.nextLine().trim()));
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (String line : logContent) {
                bw.write(line);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        addAndPrint("The log has been saved.\n");
    }
    
    private static void printHardestCard() {
        int maxMistakes = mistakeCount.values().stream().mapToInt(entry -> entry).
                filter(entry -> entry > 0).max().orElse(0);
        
        if (maxMistakes == 0) {
            addAndPrint("There are no cards with errors.\n");
            return;
        }
        
        var hardestCards = mistakeCount.entrySet().stream().filter(entry -> entry.getValue() == maxMistakes)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        if (hardestCards.size() == 0) {
            addAndPrint("There are no cards with errors.\n");
        } else if (hardestCards.size() ==  1) {
            addAndPrint(String.format("The hardest card is \"%s\". You have %d errors answering it.\n",
                    hardestCards.get(0), maxMistakes));
        } else {
            addAndPrint(String.format("The hardest cards are \"%s\". You have %d errors answering them.\n",
                    String.join("\", \"", hardestCards), maxMistakes));
        }
    }
    
    private static void resetStats() {
        mistakeCount.forEach((s, i) -> mistakeCount.put(s, 0));
        System.out.println("Card statistics has been reset.");
    }
    
    private static void addAndPrint(String s) {
        logContent.add(s);
        System.out.print(s);
    }
    
    private static String addToLog(String s) {
        logContent.add(s);
        return s;
    }
    
    private static String exportFile = null;
    
    private static void handleArgs(String[] args) {
        String importFile = null;
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-import") && i < args.length - 1) {
                importFile = args[i + 1];
            }
            if (args[i].equals("-export") && i < args.length - 1) {
                exportFile = args[i + 1];
            }
        }
        
        if (importFile != null) {
            quitImport(importFile);
        }
    }
    
    
    public static void main(String[] args) {
        handleArgs(args);
        Scanner sc = new Scanner(System.in);
        boolean exit = false;
        do {
            addAndPrint("Input the action (add, remove, import, export, ask, " +
                    "exit, log, hardest card, reset stats):\n");
            switch (addToLog(sc.nextLine().trim())) {
                case "add":
                    addCard(sc);
                    break;
                case "remove":
                    removeCard(sc);
                    break;
                case "import":
                    importCards(sc);
                    break;
                case "export":
                    exportCards(sc);
                    break;
                case "ask":
                    askUser(sc);
                    break;
                case "log":
                    printLog(sc);
                    break;
                case "hardest card":
                    printHardestCard();
                    break;
                case "reset stats":
                    resetStats();
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    addAndPrint("Wrong input\n");
                    break;
            }
            addAndPrint("\n");
        } while (!exit);
        addAndPrint("Bye bye!\n");
        if (exportFile != null) {
            quitExport(exportFile);
        }
    }
    
}
