package flashcards;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.menu();
    }
    
    private Scanner scanner = new Scanner(System.in);
    private Map<String, String> cardToDefinition = new HashMap<>();
    private Map<String, String> definitionToCard = new HashMap<>();
    public void menu(){
        while(true){
            System.out.println("Input the action (add, remove, import, export, ask, exit): ");
            switch (scanner.nextLine()) {
                case "add":
                    addCard();
                    break;
                case "remove":
                    removeCard();
                    break;
                case "import":
                    importCards();
                    break;
                case "export":
                    exportCards();
                    break;
                case "ask":
                    ask();
                    break;
                case "exit":
                    System.out.println("Bye bye!");
                    return;
                default:
                    break;
            }
        }
    }
    
    private void addCard(){
        
        System.out.println("The card: ");
        String cardName = scanner.nextLine();
        if (cardToDefinition.containsKey(cardName)) {
            System.out.println(String.format("The card \"%s\" already exists.", cardName));
            return;
        }
        System.out.println("The definition of the card: ");
        String cardDefinition = scanner.nextLine();
        if (definitionToCard.containsKey(cardDefinition)) {
            System.out.println(String.format("The definition \"%s\" already exists.", cardDefinition));
            return;
        }
        cardToDefinition.put(cardName, cardDefinition);
        definitionToCard.put(cardDefinition, cardName);
        System.out.println("The pair (\"" + cardName + "\":\"" + cardDefinition + "\") has been added.");
        
    }
    
    private void removeCard(){
        System.out.println("The card: ");
        String removeCard = scanner.nextLine();
        if (cardToDefinition.containsKey(removeCard)) {
            // the order matters:
            definitionToCard.remove(cardToDefinition.get(removeCard));
            cardToDefinition.remove(removeCard);
            
            System.out.println("Card has been removed");
        } else {
            System.out.println("Can't remove \"" + removeCard + "\": there is no such card");
        }
        
    }
    private void importCards(){
        System.out.println("File name: ");
        String file = scanner.nextLine();
        File in = new File(file);
        
        try (Scanner fileScanner = new Scanner(in)){
            int numberOfCards = 0;
            while (fileScanner.hasNextLine()) {
                String card = fileScanner.nextLine();
                String def = fileScanner.nextLine();
                
                if (cardToDefinition.containsKey(card)) {
                    definitionToCard.remove(cardToDefinition.get(card));
                }
                cardToDefinition.put(card, def);
                definitionToCard.put(def, card);
                numberOfCards++;
            }
            System.out.println(numberOfCards + " cards have been loaded.");
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        }
    }
    private void exportCards(){
        
        System.out.println("File name: ");
        File out = new File(scanner.nextLine());
        
        try (PrintWriter writer = new PrintWriter(out)){
            for (Map.Entry<String, String> cards : cardToDefinition.entrySet()) {
                writer.println(cards.getKey());
                writer.println(cards.getValue());
            }
            System.out.println(cardToDefinition.size() + " cards have been saved.");
        } catch (IOException e) {
            System.out.println("No file found");
        }
        
    }
    
    private static Random random = new Random();
    
    private void ask(){
        List<String> keys = new ArrayList<>(cardToDefinition.keySet());
        System.out.println("How many times to ask?");
        int n = Integer.parseInt(scanner.nextLine());
        for (int i = 0; i < n; i++) {
            String card = keys.get(random.nextInt(keys.size()));
            System.out.println("Print the definition of \"" + card + "\":");
            String s = scanner.nextLine();
            if (cardToDefinition.get(card).equals(s)) {
                System.out.println("Correct answer. ");
            } else if (definitionToCard.containsKey(s)){
                System.out.println("Wrong answer. (The correct one is \"" + cardToDefinition.get(card) + "\", you've just written the definition of \"" + definitionToCard.get(s) + "\" card. ");
            } else {
                System.out.println("Wrong answer. (The correct one is \"" + cardToDefinition.get(card) + "\"). ");
            }
        }
    }
}